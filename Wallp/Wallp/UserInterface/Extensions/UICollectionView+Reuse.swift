//
//  UICollectionView+Reuse.swift
//  Wallp
//
//  Created by Андрей Козлов on 12.12.2020.
//

import UIKit

extension UICollectionView {
    func register(cell: ReusableCollectionViewCell.Type) {
        register(cell, forCellWithReuseIdentifier: cell.reuseID)
    }
    
    func dequeueCell<CellType: ReusableCollectionViewCell>(for indexPath: IndexPath) -> CellType {
        return dequeueReusableCell(withReuseIdentifier: CellType.reuseID, for: indexPath) as! CellType
    }
}

class ReusableCollectionViewCell: UICollectionViewCell {
    
    static var reuseID: String {
        String(describing: Self.self)
    }
}
