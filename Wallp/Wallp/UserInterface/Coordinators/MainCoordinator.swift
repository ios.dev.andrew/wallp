//
//  MainCoordinator.swift
//  Wallp
//
//  Created by Андрей Козлов on 22.11.2020.
//

import UIKit

class MainCoordinator: Coordinator {
	
	var childCoordinators: [Coordinator] = []
	
	var onFinishFlow: (() -> Void)?

	private let navigationController: UINavigationController
	
	init(navigationController: UINavigationController) {
		self.navigationController = navigationController
	}
	
	func start() {
		let (view, output) = MainModuleConfigurator().configure()
		output.onFinish = { _ in
			
		}
		navigationController.pushViewController(view, animated: true)
	}
}
