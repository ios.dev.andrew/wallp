//
//  AppCoordinator.swift
//  Wallp
//
//  Created by Андрей Козлов on 22.11.2020.
//

import UIKit

class AppCoordinator: Coordinator {
	
	var childCoordinators: [Coordinator] = []
	
	let window: UIWindow

	init(window: UIWindow) {
		self.window = window
	}
	
	func start() {
		performMainFlow()
		window.makeKeyAndVisible()
	}
	
	private func performMainFlow() {
		let navigationController = UINavigationController()
		window.rootViewController = navigationController
		let coordinator = MainCoordinator(navigationController: navigationController)
		coordinator.onFinishFlow = { [weak self, weak coordinator] in
			self?.removeDependency(coordinator)
			self?.start()
		}
		addDependency(coordinator)
		coordinator.start()
	}
}
