//
//  Constants.swift
//  Wallp
//
//  Created by Андрей Козлов on 12.12.2020.
//

import CoreGraphics

enum Constants {
    static let padding: CGFloat = 16
}
