//
//  MainCollectionLayoutCache.swift
//  Wallp
//
//  Created by Андрей Козлов on 12.12.2020.
//

import UIKit

class MainCollectionLayoutCache {
    
    private let spacingItems: CGFloat = 16

    private let itemSize: CGSize
    private let insets: UIEdgeInsets
    private let collectionWidth: CGFloat
    private var defaultFrames = [CGRect]()

    init(insets: UIEdgeInsets, collectionWidth: CGFloat) {
        self.collectionWidth = collectionWidth
        self.insets = insets
        let itemWidth: CGFloat = (collectionWidth - spacingItems - insets.left - insets.right) / 2
        let itemHeight: CGFloat = itemWidth
        itemSize = CGSize(width: itemWidth, height: itemHeight)
     }
    
    static var zero: MainCollectionLayoutCache {
        MainCollectionLayoutCache(insets: .zero, collectionWidth: 0)
    }
    
    // MARK: - Calculation
    
    func recalculateDefaultFrames(numberOfItems: Int) {
        defaultFrames = (0..<numberOfItems).map {
            defaultCellFrame(atRow: $0)
        }
    }
    
    func defaultCellFrame(atRow row: Int) -> CGRect {
        let isLeftItem = (row % 2) == 0
        let coefficient: CGFloat
        if isLeftItem {
            coefficient = CGFloat(row / 2)
        } else {
            coefficient = CGFloat((row - 1) / 2)
        }
        return CGRect(
            x: isLeftItem ? 0 : (itemSize.width + spacingItems),
            y: (itemSize.height + spacingItems) * coefficient,
            width: itemSize.width,
            height: itemSize.height
        )
    }
    
    // MARK: - Access
    
    func visibleRows(in frame: CGRect) -> [Int] {
        defaultFrames
            .enumerated()
            .filter { $0.element.intersects(frame)}
            .map { $0.offset }
    }
    
    var contentSize: CGSize {
        CGSize(
            width: collectionWidth - (insets.left + insets.right),
            height: defaultFrames.last?.maxY ?? 0
        )
    }
}
