//
//  MainCollectionLayout.swift
//  Wallp
//
//  Created by Андрей Козлов on 12.12.2020.
//

import UIKit

class MainCollectionLayout: UICollectionViewLayout {
    
    var cache = MainCollectionLayoutCache.zero

    private let section = 0
        
    override var collectionViewContentSize: CGSize {
        cache.contentSize
    }
    
    override func prepare() {
        super.prepare()
        let numberOfItems = collectionView!.numberOfItems(inSection: section)
        
        cache = MainCollectionLayoutCache(
            insets: collectionView!.contentInset,
            collectionWidth: collectionView!.bounds.width
        )
        cache.recalculateDefaultFrames(numberOfItems: numberOfItems)
    }
    
    override func layoutAttributesForElements(in rect: CGRect) -> [UICollectionViewLayoutAttributes]? {
        let indexes = cache.visibleRows(in: rect)
        
        let cells = indexes.map { (row) -> UICollectionViewLayoutAttributes? in
            let path = IndexPath(row: row, section: section)
            let attributes = layoutAttributesForItem(at: path)
            return attributes
        }
        .compactMap { $0 }
        .filter { $0.representedElementCategory == .cell }
                                
        return cells
    }
    
    override func layoutAttributesForItem(at indexPath: IndexPath) -> UICollectionViewLayoutAttributes? {
        let attributes = UICollectionViewLayoutAttributes(forCellWith: indexPath)
        attributes.frame = cache.defaultCellFrame(atRow: indexPath.row)
        return attributes
    }
}
