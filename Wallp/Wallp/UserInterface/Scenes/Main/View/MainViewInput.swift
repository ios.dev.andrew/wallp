//
//  MainViewInput.swift
//  Wallp
//
//  Created by Andrew Kozlov on 22/11/2020.
//  Copyright © 2020 Fikus. All rights reserved.
//

protocol MainViewInput: class {
    /// Method for setup initial state of view
    func setupInitialState()
}
