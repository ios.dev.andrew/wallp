//
//  MainModuleOutput.swift
//  Wallp
//
//  Created by Andrew Kozlov on 22/11/2020.
//  Copyright © 2020 Fikus. All rights reserved.
//

enum MainOutputModel {
	case next
}

typealias MainOutputBlock = (MainOutputModel) -> Void

protocol MainModuleOutput: class {
	var onFinish: MainOutputBlock? { get set }
}
