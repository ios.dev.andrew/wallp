//
//  MainPresenter.swift
//  Wallp
//
//  Created by Andrew Kozlov on 22/11/2020.
//  Copyright © 2020 Fikus. All rights reserved.
//

final class MainPresenter: MainViewOutput, MainModuleInput, MainModuleOutput {
	
    // MARK: - MainModuleOutput

	var onFinish: MainOutputBlock?

    // MARK: - Properties

    weak var view: MainViewInput?

    // MARK: - MainViewOutput

    func viewLoaded() {
        view?.setupInitialState()
    }

    // MARK: - MainModuleInput

}

