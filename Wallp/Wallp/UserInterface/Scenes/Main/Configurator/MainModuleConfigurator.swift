//
//  MainModuleConfigurator.swift
//  Wallp
//
//  Created by Andrew Kozlov on 22/11/2020.
//  Copyright © 2020 Fikus. All rights reserved.
//

import UIKit

final class MainModuleConfigurator {

    // MARK: - Internal methods

    func configure() -> (MainViewController, MainModuleOutput) {
        let view = MainViewController()
        let presenter = MainPresenter()

        presenter.view = view
        view.output = presenter

        return (view, presenter)
    }

}
