//
//  WallpCollectionViewCell.swift
//  Wallp
//
//  Created by Андрей Козлов on 12.12.2020.
//

import UIKit

class WallpCollectionViewCell: ReusableCollectionViewCell {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        contentView.backgroundColor = .lightGray
        contentView.rounded(radius: frame.height / 4)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
